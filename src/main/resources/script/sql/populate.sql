START TRANSACTION;
	USE ANSQUESTDB;
	
	INSERT INTO QUESTION(TEXT) 
		VALUES('L''italia dal punto di vista politico �:');
	
	INSERT INTO ANSWER(TEXT) VALUES('Una republica');
	INSERT INTO ANSWER(TEXT) VALUES('Una monarchia');
	INSERT INTO ANSWER(TEXT) VALUES('Una monarchia parlamentare');
	INSERT INTO ANSWER(TEXT) VALUES('Una dittatura');
	    
	INSERT INTO QUESTION_X_ANSWER(ID_QUESTION, ID_ANSWER, EXACT_ANSWER)
		VALUES(1,1,1);
	INSERT INTO QUESTION_X_ANSWER(ID_QUESTION, ID_ANSWER)
		VALUES(1,2);
	INSERT INTO QUESTION_X_ANSWER(ID_QUESTION, ID_ANSWER)
		VALUES(1,3);
	INSERT INTO QUESTION_X_ANSWER(ID_QUESTION, ID_ANSWER)
		VALUES(1,4);
	
	INSERT INTO QUESTION(TEXT) 
		VALUES('In Informatica cosa identifica la sigla RAM');
	
	INSERT INTO ANSWER(TEXT) VALUES('Random Access Memory');
	INSERT INTO ANSWER(TEXT) VALUES('Rainbow Access Mail');
	INSERT INTO ANSWER(TEXT) VALUES('Rider Allow Mail');
	INSERT INTO ANSWER(TEXT) VALUES('Random Access Monitor');
	    
	INSERT INTO QUESTION_X_ANSWER(ID_QUESTION, ID_ANSWER, EXACT_ANSWER)
		VALUES(2,5,1);
	INSERT INTO QUESTION_X_ANSWER(ID_QUESTION, ID_ANSWER)
		VALUES(2,6);
	INSERT INTO QUESTION_X_ANSWER(ID_QUESTION, ID_ANSWER)
		VALUES(2,7);
	INSERT INTO QUESTION_X_ANSWER(ID_QUESTION, ID_ANSWER)
		VALUES(2,8);
COMMIT

START TRANSACTION
	INSERT INTO QUESTIONNAIRE(NAME) 
		VALUES('Questionnaire A');
	
	INSERT INTO QUESTIONNAIRE_X_QUESTION(ID_QUESTIONNAIRE, ID_QUESTION)
		VALUES(1, 1);
	    
	INSERT INTO QUESTIONNAIRE_X_QUESTION(ID_QUESTIONNAIRE, ID_QUESTION)
		VALUES(1, 2);
COMMIT;