START TRANSACTION; 

CREATE DATABASE IF NOT EXISTS ansQuestDb;
USE ANSQUESTDB;

-- questionnaire table def
CREATE TABLE QUESTIONNAIRE (
	ID 			INT AUTO_INCREMENT,
    NAME		VARCHAR(255) 		DEFAULT 'UNNAMED',
    AVALAIBLE	BIT 				DEFAULT TRUE,
    
    PRIMARY KEY(ID)
);

-- questionnaire table question
CREATE TABLE QUESTION (
	ID 				 INT AUTO_INCREMENT,
    TEXT			 VARCHAR(255)		UNIQUE NOT NULL,
    AVALAIBLE		 BIT 				DEFAULT TRUE,
    MULTIPLE_CHOICE  BIT				DEFAULT FALSE,
    POINT			 NUMERIC(4,2)		DEFAULT 1.0,
    
    PRIMARY KEY(ID)
);

-- questionnaire table questionnaire_x_question def
CREATE TABLE QUESTIONNAIRE_X_QUESTION (
	ID_QUESTIONNAIRE	INT,
    ID_QUESTION			INT,
    
    PRIMARY KEY(ID_QUESTIONNAIRE, ID_QUESTION)
);

--
--
--
--   ID    	 											   ID
--   _|__________________								   _|__________________
--  |					 |	(1,N)				(1,N)	  |					   |
--  |	QUESTIONNAIRE	 |---------------<>---------------|      QUEESTION     |
--  |____________________|				 				  |____________________|	
--										 
--
--

-- questionnaire table answer
CREATE TABLE ANSWER (
	ID 				 INT AUTO_INCREMENT,
    TEXT			 VARCHAR(255) 		UNIQUE NOT NULL,
    
    PRIMARY KEY(ID)
 );

-- questionnaire table question_x_answer def
CREATE TABLE QUESTION_X_ANSWER (
	ID_QUESTION			INT,
    ID_ANSWER			INT,
    EXACT_ANSWER		BIT				DEFAULT FALSE,

    PRIMARY KEY(ID_QUESTION, ID_ANSWER)
);

--
--
--   ID    	 											   ID		  ID_QUESTION
--   _|__________________								   _|______________|___
--  |					 |	(1,N)				(1,N)	  |					   |
--  |		QUESTION	 |---------------<>---------------|   	 ANSWER 	   |
--  |____________________|								  |____________________|	
--
--
--
--

