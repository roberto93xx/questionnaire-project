package it.ra.repository;

import it.ra.entity.Questionnaire;

public interface QuestionnaireRepository extends BasicCrudRepository<Questionnaire> {
	
}
