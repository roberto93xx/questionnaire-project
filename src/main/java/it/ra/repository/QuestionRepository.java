package it.ra.repository;

import it.ra.entity.Question;

public interface QuestionRepository extends BasicCrudRepository<Question> {
	
}
