package it.ra.repository.impl;

import java.util.List;
import java.util.logging.Logger;

import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;
import javax.transaction.Transactional.TxType;
import javax.transaction.UserTransaction;

import it.ra.cdi.qualifier.QuestionRepositoryQualifier;
import it.ra.entity.Question;
import it.ra.repository.QuestionRepository;

@ApplicationScoped
@TransactionManagement(value = TransactionManagementType.CONTAINER)
@QuestionRepositoryQualifier
public class QuestionRepositoryImpl implements QuestionRepository {
	
	@Inject
	private Logger LOG;
	@Inject
	private UserTransaction userTransaction;
	@Inject
	private EntityManager entityManager;
	
	@Override
	public List<Question> selectAll() {
		LOG.info("selectAll -START");
		
		TypedQuery<Question> query = this.entityManager.createNamedQuery("getAllQuestions", Question.class);
		List<Question> result = query.getResultList();
		
		LOG.info(String.format("selectAll - retrueve %s question END", result == null? 0: result.size()));
		return result;
	}

	@Override
	public Question selectById(Integer id) {
		return null;
	}

	@Override
	@Transactional(value = TxType.REQUIRED ,rollbackOn = Exception.class)
	public boolean save(Question question) {
		LOG.info(String.format("save - %s - START", question));
		boolean result = false;
		
		this.entityManager.persist(question);
		result = this.entityManager.contains(question);
		
		LOG.info("save - END");
		return result;
	}

	@Override
	@Transactional(value = TxType.REQUIRED ,rollbackOn = Exception.class)
	public void update(Question t) {
		// TODO Auto-generated method stub
		
	}

	@Override
	@Transactional(value = TxType.REQUIRED ,rollbackOn = Exception.class)
	public void delete(Question t) {
		// TODO Auto-generated method stub
		
	}
	
}