package it.ra.repository.impl;

import java.util.List;

import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.transaction.UserTransaction;

import it.ra.cdi.qualifier.QuestionnaireRepositoryQualfier;
import it.ra.entity.Questionnaire;
import it.ra.repository.QuestionnaireRepository;

@ApplicationScoped
@TransactionManagement(value = TransactionManagementType.BEAN)
@QuestionnaireRepositoryQualfier
public class QuestionnaireRepositoryImpl implements QuestionnaireRepository {
	
	@Inject
	private UserTransaction userTransaction;

	@Override
	public List<Questionnaire> selectAll() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Questionnaire selectById(Integer id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean save(Questionnaire t) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void update(Questionnaire t) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void delete(Questionnaire t) {
		// TODO Auto-generated method stub
		
	}
	
}