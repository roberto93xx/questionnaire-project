package it.ra.repository.impl.mock;

import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.transaction.UserTransaction;

import it.ra.cdi.qualifier.QuestionMockRepositoryQualifier;
import it.ra.entity.Question;
import it.ra.repository.QuestionRepository;

@ApplicationScoped
@QuestionMockRepositoryQualifier
public class QuestionRepositoryMockImpl implements QuestionRepository {
	
	@Inject
	private UserTransaction userTransaction;

	@Override
	public List<Question> selectAll() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Question selectById(Integer id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean save(Question t) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void update(Question t) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void delete(Question t) {
		// TODO Auto-generated method stub
		
	}

	
}
