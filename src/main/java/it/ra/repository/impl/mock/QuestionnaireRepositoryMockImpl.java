package it.ra.repository.impl.mock;

import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.transaction.UserTransaction;

import it.ra.cdi.qualifier.QuestionnaireMockRepositoryQualifier;
import it.ra.cdi.qualifier.QuestionnaireRepositoryQualfier;
import it.ra.entity.Question;
import it.ra.entity.Questionnaire;
import it.ra.repository.QuestionRepository;
import it.ra.repository.QuestionnaireRepository;

@ApplicationScoped
@QuestionnaireMockRepositoryQualifier
public class QuestionnaireRepositoryMockImpl implements QuestionnaireRepository {
	
	@Inject
	private UserTransaction userTransaction;

	@Override
	public List<Questionnaire> selectAll() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Questionnaire selectById(Integer id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean save(Questionnaire t) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void update(Questionnaire t) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void delete(Questionnaire t) {
		// TODO Auto-generated method stub
		
	}
	
}