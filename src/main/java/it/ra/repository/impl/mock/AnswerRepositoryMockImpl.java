package it.ra.repository.impl.mock;

import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.transaction.UserTransaction;

import it.ra.cdi.qualifier.AnswerMockRepositoryQualifier;
import it.ra.entity.Answer;
import it.ra.entity.Question;
import it.ra.repository.AnswerRepository;
import it.ra.repository.QuestionRepository;

@ApplicationScoped
@AnswerMockRepositoryQualifier
public class AnswerRepositoryMockImpl implements AnswerRepository {
	
	@Inject
	private UserTransaction userTransaction;

	@Override
	public List<Answer> selectAll() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Answer selectById(Integer id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean save(Answer t) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void update(Answer t) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void delete(Answer t) {
		// TODO Auto-generated method stub
		
	}
	
}