package it.ra.repository.impl;

import java.util.List;

import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.transaction.UserTransaction;

import it.ra.cdi.qualifier.AnswerRepositoryQualifier;
import it.ra.entity.Answer;
import it.ra.repository.AnswerRepository;

@ApplicationScoped
@TransactionManagement(value = TransactionManagementType.BEAN)
@AnswerRepositoryQualifier
public class AnswerRepositoryImpl implements AnswerRepository {
	
	@Inject
	private UserTransaction userTransaction;

	@Override
	public List<Answer> selectAll() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Answer selectById(Integer id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean save(Answer t) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void update(Answer t) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void delete(Answer t) {
		// TODO Auto-generated method stub
		
	}
	
}