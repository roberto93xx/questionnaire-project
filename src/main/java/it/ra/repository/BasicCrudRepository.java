package it.ra.repository;

import java.util.List;

public interface BasicCrudRepository<T> {
	public List<T> selectAll();
	
	public T selectById(Integer id);
	
	public boolean save(T t);
	
	public void update(T t);
	
	public void delete(T t);
}
