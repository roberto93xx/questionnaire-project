package it.ra.repository;

import it.ra.entity.Answer;

public interface AnswerRepository extends BasicCrudRepository<Answer> {

}
