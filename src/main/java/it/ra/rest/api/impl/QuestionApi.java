package it.ra.rest.api.impl;

import java.util.List;
import java.util.logging.Logger;

import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import it.ra.entity.Question;
import it.ra.rest.api.BasicRestApi;
import it.ra.service.QuestionService;

@Path(value = "/question")
@Produces(value = MediaType.APPLICATION_JSON)
@Consumes(value = MediaType.APPLICATION_JSON)
public class QuestionApi implements BasicRestApi<Question> {

	private static final String BASE_REST = "/rest/api/";
	private static final String BASE_PATH = "/question";

	@Inject
	private Logger LOG;
	@Inject
	private QuestionService questionService;

	
	@GET
	@PermitAll
	@Override
	public Response getAll() {
		LOG.info(String.format(">>> GET url=%s", BASE_REST.concat(BASE_PATH)));
		LOG.info("getAll - START");
		Response response = null;

		List<Question> theAnswers = this.questionService.getAll();
		response = Response.status(Status.OK).entity(theAnswers).build();

		LOG.info("getAll - END");
		return response;
	}

	@GET
	@Path(value = "/{id}")
	@Override
	public Response getById(@PathParam(value = "id") Integer id) {
		LOG.info(String.format(">>> GET url=%s", BASE_REST.concat(BASE_PATH).concat("/").concat(id.toString())));
		LOG.info(String.format("getById id=%s - START", id));
		Response response = null;

		Question theQuestion = this.questionService.getById(id);
		if (theQuestion == null) {
			response = Response.status(Status.NOT_FOUND).build();

		} else {
			response = Response.status(Status.OK).entity(theQuestion).build();
		}

		LOG.info("getById - END");
		return response;
	}

	@POST
	@RolesAllowed(value = {"user"})
	@Override
	public Response save(Question question) {
		LOG.info(String.format(">>> POST url=%s", BASE_REST.concat(BASE_PATH)));
		LOG.info(String.format("save %s - START", question));
		Response response = null;

		boolean saved = this.questionService.save(question);
		if (saved) {
			response = Response.status(Status.OK).entity(String.format("Question saved with success")).build();
		} else {
			response = Response.status(Status.OK).entity("Question not saved with success").build();
		}

		LOG.info("save - END");
		return response;
	}

	@PUT
	@Override
	public Response update(Question t) {
		LOG.info(String.format(">>> PUT url=%s", BASE_REST.concat(BASE_PATH)));
		// TODO Auto-generated method stub
		return null;

	}

	@DELETE
	@Path(value = "/{id}")
	@Override
	public Response delete(@PathParam(value = "id") Integer id) {
		LOG.info(String.format(">>> DELETE url=%s", BASE_REST.concat(BASE_PATH).concat("/").concat(id.toString())));
		// TODO Auto-generated method stub
		return null;
	}

}
