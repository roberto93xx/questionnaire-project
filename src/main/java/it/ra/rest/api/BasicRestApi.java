package it.ra.rest.api;

import javax.ws.rs.core.Response;

public interface BasicRestApi<T> {
	public Response getAll();
	
	public Response getById(Integer id);
	
	public Response save(T t);
	
	public Response update(T t);
	
	public Response delete(Integer id);
}