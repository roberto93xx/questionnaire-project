package it.ra.rest.config;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

@ApplicationPath(value = "/rest/api")
public class RestConfig extends Application{ }
