package it.ra.entity;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "ANSWER", schema = "ANSQUESTDB")
@NamedQueries(value = {
	@NamedQuery(name = "getAll", 										query = "SELECT a FROM Answer a"),
	@NamedQuery(name = "getById", 										query = "SELECT a FROM Answer a WHERE a.id = :theId"),
	@NamedQuery(name = "getAllByQuestionId", 							query = "SELECT a FROM Answer a JOIN a.questions questXAns WHERE questXAns.questionXAnswerId.questionId = :theQuestionId"),
	@NamedQuery(name = "getAllAnswerAndRelativeQuestionsByAnswerId", 	query = "SELECT a FROM Answer a JOIN FETCH a.questions  WHERE a.id = :theId")
})
public class Answer implements Serializable {
	private static final long serialVersionUID = -3164265292241593731L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID")
	private Integer id;
	@Column(name = "TEXT", unique = true)
	@NotNull(message = "name cannot be null")
	@Size(min = 6, max = 255)
	private String text;
	@JsonIgnore
	@OneToMany(mappedBy = "answer", fetch = FetchType.LAZY)
	private Set<QuestionXAnswer> questions;

	public Answer() {
		super();
	}
}