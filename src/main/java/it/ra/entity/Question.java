package it.ra.entity;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "QUESTION", schema = "ANSQUESTDB")
@NamedQueries(value = { 
	@NamedQuery(name = "getAllQuestions",						query = "SELECT q FROM Question q"), 
	@NamedQuery(name = "getAllQuestionsAndRelativeAnswers", 	query = "SELECT q FROM Question q LEFT JOIN FETCH q.answers"), 
	@NamedQuery(name = "getQuestionById", 						query = "SELECT q FROM Question q WHERE q.id = :theId"), 
	@NamedQuery(name = "getQuestionByIdAndRelativeAnswers", 	query = "SELECT q FROM Question q LEFT JOIN FETCH q.answers WHERE q.id = :theId"), 
//	@NamedQuery(name = "getAllExactAnswersByQuestionId", 		query = "SELECT q FROM Question q LEFT JOIN FETCH q.answers WHERE q.answers.exactAnswer = :isExact")
	@NamedQuery(name = "getAllMultipleChoiceQuestions", 		query = "SELECT q FROM Question q WHERE q.multipleChoice = :isMultipleChoice"),
	@NamedQuery(name = "getAllQuestionsOrderByPointDesc", 		query = "SELECT q FROM Question q ORDER BY q.point"),
	@NamedQuery(name = "getAllQuestionsOrderByExactAnswerDesc", query = "SELECT q FROM Question q LEFT JOIN q.answers qa ORDER BY qa.exactAnswer DESC")
})
public class Question implements Serializable {
	private static final long serialVersionUID = 4000833227544681906L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID")
	private Integer id;
	@Column(name = "TEXT", unique = true)
	@NotNull(message = "name cannot be null")
	@Size(min = 6, max = 255)
	private String text;
	@Column(name = "AVALAIBLE")
	@NotNull
	private boolean avalaible;
	@Column(name = "MULTIPLE_CHOICE")
	private boolean multipleChoice;
	@Column(name = "point")
	@Digits(integer = 2, fraction = 2)
	@DecimalMin(value = "1.00")
	@DecimalMax(value = "4.00")
	private float point;
	@JsonIgnore
	@ManyToMany(mappedBy = "questions")
	private Set<Questionnaire> questionnaires;
	@JsonIgnore
	@OneToMany(mappedBy = "question", fetch = FetchType.LAZY)
	private Set<QuestionXAnswer> answers;

	public Question() {
		super();
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public boolean isAvalaible() {
		return avalaible;
	}

	public void setAvalaible(boolean avalaible) {
		this.avalaible = avalaible;
	}

	public boolean isMultipleChoice() {
		return multipleChoice;
	}

	public void setMultipleChoice(boolean multipleChoice) {
		this.multipleChoice = multipleChoice;
	}

	public float getPoint() {
		return point;
	}

	public void setPoint(float point) {
		this.point = point;
	}

	public Set<Questionnaire> getQuestionnaires() {
		return questionnaires;
	}

	public void setQuestionnaires(Set<Questionnaire> questionnaires) {
		this.questionnaires = questionnaires;
	}

	public Set<QuestionXAnswer> getAnswers() {
		if (this.answers == null) {
			this.answers = new HashSet<>();
		}
		return answers;
	}

	public void setAnswers(Set<QuestionXAnswer> questionXAnswer) {
		this.answers = questionXAnswer;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((text == null) ? 0 : text.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Question other = (Question) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (text == null) {
			if (other.text != null)
				return false;
		} else if (!text.equals(other.text))
			return false;
		return true;
	}

}