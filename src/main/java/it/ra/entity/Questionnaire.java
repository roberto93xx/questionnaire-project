package it.ra.entity;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "QUESTIONNAIRE", schema = "ANSQUESTDB")
@NamedQueries(value = {
	@NamedQuery(name = "getAllQuestionnaires", 									query = "SELECT q FROM Questionnaire q"),
	@NamedQuery(name = "getAllQuestionnaireAvalible",	 						query = "SELECT q FROM Questionnaire q WHERE q.id = :theId"),
	@NamedQuery(name = "getQuestionnaireById",	 								query = "SELECT q FROM Questionnaire q WHERE q.avalaible = :isAvlaible"),
	@NamedQuery(name = "getQuestionnaireByIdAndRelativeQuestions",	 			query = "SELECT q FROM Questionnaire q LEFT JOIN FETCH q.questions WHERE q.id = :theId"),
	@NamedQuery(name = "getQuestionnaireByIdAndRelativeQuestionsAnswers",	 	query = "SELECT q FROM Questionnaire q LEFT JOIN q.questions qq LEFT JOIN  qq.answers a WHERE q.id = :theId"),
	@NamedQuery(name = "getQuestionsByQuestionnaireId",	 						query = "SELECT q FROM Questionnaire q LEFT JOIN FETCH q.questions WHERE q.id = :theId")
//	@NamedQuery(name = "getAnswersByQuestionnaireId",	 						query = "SELECT a FROM Questionnaire q LEFT JOIN q.questions qq LEFT JOIN FETCH qq.answers a WHERE q.id = :theId")
})
public class Questionnaire implements Serializable {
	private static final long serialVersionUID = 1412865314910169413L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID")
	private Integer id;
	@Column(name = "NAME", unique = true)
	@NotNull(message = "name cannot be null")
	@Size(min = 15, max = 199)
	@Pattern(regexp = "[Q q]uestionnaire *")
	private String name;
	@Column(name = "AVALAIBLE")
	@NotNull
	private boolean avalaible;
	@JsonIgnore
	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(
		name = "QUESTIONNAIRE_X_QUESTION", 
		joinColumns = @JoinColumn(name = "ID_QUESTIONNAIRE"),
		inverseJoinColumns = @JoinColumn(name = "ID_QUESTION")
	)
	private Set<Question> questions;

	public Questionnaire() {
		super();
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean isAvalaible() {
		return avalaible;
	}

	public void setAvalaible(boolean avalaible) {
		this.avalaible = avalaible;
	}

	public Set<Question> getQuestions() {
		if(this.questions == null) {
			this.questions = new HashSet<>();
		}
		return questions;
	}

	public void setQuestions(Set<Question> questions) {
		this.questions = questions;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Questionnaire other = (Questionnaire) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

}
