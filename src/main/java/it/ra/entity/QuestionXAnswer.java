package it.ra.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "QUESTION_X_ANSWER", schema = "ANSQUESTDB")
public class QuestionXAnswer implements Serializable {
	private static final long serialVersionUID = -3164265292241593731L;

	@EmbeddedId
	private QuestionXAnswerPK questionXAnswerId;
	@JsonIgnore
	@ManyToOne(targetEntity = Question.class, fetch = FetchType.EAGER)
	@JoinColumn(name = "ID_QUESTION", insertable = false, updatable = false)
	private Question question;
	@JsonIgnore
	@ManyToOne(targetEntity = Answer.class,   fetch = FetchType.EAGER)
	@JoinColumn(name = "ID_ANSWER", insertable = false, updatable = false)
	private Answer answer;
	@Column(name = "EXACT_ANSWER")
	@NotNull
	private boolean exactAnswer;

	public QuestionXAnswer() {
		super();
	}

	public QuestionXAnswerPK getQuestionXAnswerId() {
		return questionXAnswerId;
	}

	public void setQuestionXAnswerId(QuestionXAnswerPK questionXAnswerId) {
		this.questionXAnswerId = questionXAnswerId;
	}

	public boolean isExactAnswer() {
		return exactAnswer;
	}

	public void setExactAnswer(boolean exactAnswer) {
		this.exactAnswer = exactAnswer;
	}

	public Answer getAnswer() {
		return answer;
	}

	public void setAnswer(Answer answer) {
		this.answer = answer;
	}

	public Question getQuestion() {
		return question;
	}

	public void setQuestion(Question question) {
		this.question = question;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((questionXAnswerId == null) ? 0 : questionXAnswerId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		QuestionXAnswer other = (QuestionXAnswer) obj;
		if (questionXAnswerId == null) {
			if (other.questionXAnswerId != null)
				return false;
		} else if (!questionXAnswerId.equals(other.questionXAnswerId))
			return false;
		return true;
	}
}