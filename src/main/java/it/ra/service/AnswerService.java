package it.ra.service;

import it.ra.entity.Answer;

public interface AnswerService extends BasicCrudService<Answer> {

}
