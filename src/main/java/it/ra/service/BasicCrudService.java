package it.ra.service;

import java.util.List;

public interface BasicCrudService<T> {
	
	public List<T> getAll();
	
	public T getById(Integer id);
	
	public boolean save(T t);

}
