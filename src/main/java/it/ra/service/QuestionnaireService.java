package it.ra.service;

import it.ra.entity.Questionnaire;

public interface QuestionnaireService extends BasicCrudService<Questionnaire> {

}
