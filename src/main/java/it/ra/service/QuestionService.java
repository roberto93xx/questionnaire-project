package it.ra.service;

import it.ra.entity.Question;

public interface QuestionService extends BasicCrudService<Question> {

}
