package it.ra.service.impl;

import java.util.List;
import java.util.logging.Logger;

import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.transaction.Transactional.TxType;

import it.ra.cdi.qualifier.QuestionnaireRepositoryQualfier;
import it.ra.entity.Questionnaire;
import it.ra.repository.QuestionnaireRepository;
import it.ra.service.QuestionnaireService;

@Stateless
@Local
public class QuestionnaireServiceImpl implements QuestionnaireService {

	@Inject
	private Logger LOG;
	@Inject
	@QuestionnaireRepositoryQualfier
	private QuestionnaireRepository questionnaireRepo;
	
	@Override
	public List<Questionnaire> getAll() {
		LOG.info("getAll - START");

		List<Questionnaire> theAnswerList = this.questionnaireRepo.selectAll();
		
		LOG.info("getAll - END");
		return theAnswerList;
	}

	@Override
	public Questionnaire getById(Integer id) {
		LOG.info(String.format("getByID id=%s - START", id));

		Questionnaire theQuestionnaire = this.questionnaireRepo.selectById(id);
		
		LOG.info("getById - END");
		return theQuestionnaire;
	}

	@Override
	@Transactional(value = TxType.REQUIRED, rollbackOn = Exception.class)
	public boolean save(Questionnaire questionnaire) {
		LOG.info(String.format("save %s - START", questionnaire));
		
		boolean saved = this.questionnaireRepo.save(questionnaire);
		
		LOG.info("save - END");
		return saved;
	}
}