package it.ra.service.impl;

import java.util.List;
import java.util.logging.Logger;

import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.transaction.Transactional.TxType;

import it.ra.cdi.qualifier.AnswerRepositoryQualifier;
import it.ra.entity.Answer;
import it.ra.repository.AnswerRepository;
import it.ra.service.AnswerService;

@Stateless
@Local
public class AnswerServiceImpl implements AnswerService {

	@Inject
	private Logger LOG;
	@Inject
	@AnswerRepositoryQualifier
	private AnswerRepository answerRepo;
	
	@Override
	public List<Answer> getAll() {
		LOG.info("getAll - START");

		List<Answer> theAnswerList = this.answerRepo.selectAll();
		
		LOG.info("getAll - END");
		return theAnswerList;
	}

	@Override
	public Answer getById(Integer id) {
		LOG.info(String.format("getByID id=%s - START", id));

		Answer theAnswer = this.answerRepo.selectById(id);
		
		LOG.info("getById - END");
		return theAnswer;
	}

	@Override
	@Transactional(value = TxType.REQUIRED, rollbackOn = Exception.class)
	public boolean save(Answer answer) {
		LOG.info(String.format("save %s - START", answer));
		
		boolean saved = this.answerRepo.save(answer);
		
		LOG.info("save - END");
		return saved;
	}
}