package it.ra.service.impl;

import java.util.List;
import java.util.logging.Logger;

import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.transaction.Transactional.TxType;

import it.ra.cdi.qualifier.QuestionRepositoryQualifier;
import it.ra.entity.Question;
import it.ra.repository.QuestionRepository;
import it.ra.service.QuestionService;

@Stateless
@Local
public class QuestionServiceImpl implements QuestionService {

	@Inject
	private Logger LOG;
	@Inject
	@QuestionRepositoryQualifier
	private QuestionRepository questionRepository;
	
	@Override
	public List<Question> getAll() {
		LOG.info("getAll - START");

		List<Question> theAnswerList = this.questionRepository.selectAll();
		
		LOG.info("getAll - END");
		return theAnswerList;
	}

	@Override
	public Question getById(Integer id) {
		LOG.info(String.format("getByID id=%s - START", id));

		Question theQuestion = this.questionRepository.selectById(id);
		
		LOG.info("getById - END");
		return theQuestion;
	}

	@Override
	@Transactional(value = TxType.REQUIRED, rollbackOn = Exception.class)
	public boolean save(Question question) {
		LOG.info(String.format("save %s - START", question));
		
		boolean saved = this.questionRepository.save(question);
		
		LOG.info("save - END");
		return saved;
	}
	
}
