package it.ra.cdi.util;

import java.util.logging.Logger;

import javax.enterprise.inject.Produces;
import javax.enterprise.inject.spi.InjectionPoint;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

public class ResourceBean {

	@Produces
	@PersistenceContext(name = "entityManager", unitName = "answerQuestionPU")
	private EntityManager entityManeger;
	
	@Produces
	private Logger getLogger(InjectionPoint injectionPoint) {
		return Logger.getLogger(injectionPoint.getMember().getDeclaringClass().getSimpleName());
	}
	
}